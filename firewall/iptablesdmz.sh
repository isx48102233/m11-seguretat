#! /bin/bash
# iptables01
# @edt ASIX-M11
# --------------------
# Regles flush: buidar regles actuals
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Establir politiques per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD DROP
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# Obrir trafic propi localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Obrir trafic propi ip publica
iptables -A INPUT -s 192.168.2.41 -j ACCEPT
iptables -A OUTPUT -d 192.168.2.41 -j ACCEPT

# EXERCICI 1 PORT-FORWARDING DE HR A DMZ
	#echo 1 > /proc/sys/net/ipv4/ip_forward

	#iptables -t nat -A PREROUTING -p tcp -s 0.0.0.0/0 --dport 4000 -j DNAT --to 10.0.0.2:13
	# La seguent aplica reject perque no hi es especificat el FORWARD-
	#iptables -t nat -A PREROUTING -p tcp -s 0.0.0.0/0 --dport 4001 -j DNAT --to 172.40.0.2:13

	#iptables -A FORWARD -d 10.0.0.0/8 -s 0.0.0.0/0 -i enp5s0 -j ACCEPT
	#iptables -A FORWARD -s 10.0.0.0/8 -d 0.0.0.0/0 -o enp5s0 -m state --state RELATED,ESTABLISHED -j ACCEPT
	#iptables -A FORWARD -j REJECT

# EXERCICI 2- Xapar extern a A i B.

	#iptables -A FORWARD -s 0.0.0.0/0 -i enp5s0 -d 172.40.0.0/16 -j REJECT
	#iptables -A FORWARD -s 0.0.0.0/0 -i enp5s0 -d 172.41.0.0/16 -j REJECT

# EXERCICI 3- A i B NAT ( DMZ TAMBE EXAM)

	#iptables -t nat -A POSTROUTING -s 172.41.0.0/16 -o enp5s0 -j MASQUERADE
	#iptables -t nat -A POSTROUTING -s 172.42.0.0/16 -o enp5s0 -j MASQUERADE
	#iptables -t nat -A POSTROUTING -s 10.0.0.0/8 -o enp5s0 -j MASQUERADE
	#
	#
	#iptables -A FORWARD -d 0.0.0.0/0 -o enp5s0 -s 172.41.0.0/16 -j ACCEPT
	#iptables -A FORWARD -d 0.0.0.0/0 -o enp5s0 -s 172.42.0.0/16 -j ACCEPT
	#iptables -A FORWARD -d 0.0.0.0/0 -o enp5s0 -s 10.0.0.0/8 -j ACCEPT
	#
	#iptables -A FORWARD -s 0.0.0.0/0 -i enp5s0 -d 172.41.0.0/16 -m state --state RELATED,ESTABLISHED -j ACCEPT
	#iptables -A FORWARD -s 0.0.0.0/0 -i enp5s0 -d 172.42.0.0/16 -m state --state RELATED,ESTABLISHED -j ACCEPT
	#iptables -A FORWARD -s 0.0.0.0/0 -i enp5s0 -d 10.0.0.0/8 -m state --state RELATED,ESTABLISHED -j ACCEPT
	#
	#iptables -A FORWARD -j REJECT

# EXERCICI 4- A i B poden accedir als serveis S1, S2, S3

	#iptables -A FORWARD -s 172.40.0.0/16 -d 10.0.0.2 -j ACCEPT
	#iptables -A FORWARD -s 10.0.0.2 -d 172.40.0.0/16 -m state --state RELATED,ESTABLISHED -j ACCEPT
	#iptables -A FORWARD -s 172.41.0.0/16 -d 10.0.0.2 -j ACCEPT
	#iptables -A FORWARD -s 10.0.0.2 -d 172.41.0.0/16 -m state --state RELATED,ESTABLISHED -j ACCEPT
	#
	#iptables -A FORWARD -j REJECT

# EXERCICI 5- Exercicis entre A i B (mes per exam)

	## XAPAR TOTS ELS DAYTIME DE LA XARXA Y DESDE X
	#iptables -A FORWARD -p tcp -s 172.40.0.0/16 -d 172.41.0.0/16 --dport 13 -j REJECT
	#
	#iptables -A FORWARD -s 172.41.0.0/16 -d 172.40.0.0/16 -j ACCEPT
	#iptables -A FORWARD -s 172.40.0.0/16 -d 172.41.0.0/16 -m state --state RELATED,ESTABLISHED -j ACCEPT
	#
	#iptables -A FORWARD -s 172.40.0.0/16 -d 172.41.0.0/16 -j ACCEPT
	#iptables -A FORWARD -s 172.41.0.0/16 -d 172.40.0.0/16 -m state --state RELATED,ESTABLISHED -j ACCEPT

# EXERCICI 6: ADMIN desde casa pot ssh a A, B , DMZ (port forwarding a HR..)

iptables -A FORWARD -p tcp -s 192.168.2.42 -i enp5s0 --dport 5022:7022 -j ACCEPT
iptables -A FORWARD -p tcp -s 192.168.2.42 -i enp5s0 -j REJECT

iptables -t nat -A PREROUTING -p tcp -s 192.168.2.42 --dport 5022 -j DNAT --to 10.0.0.2:22
iptables -t nat -A PREROUTING -p tcp -s 192.168.2.42 --dport 6022 -j DNAT --to 172.40.0.2:22
iptables -t nat -A PREROUTING -p tcp -s 192.168.2.42 --dport 7022 -j DNAT --to 172.41.0.2:22




