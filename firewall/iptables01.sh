#! /bin/bash
# iptables01
# @edt ASIX-M11
# --------------------
# Regles flush: buidar regles actuals
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Establir politiques per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# Obrir trafic propi localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Obrir trafic propi ip publica
iptables -A INPUT -s 192.168.2.41 -j ACCEPT
iptables -A OUTPUT -d 192.168.2.41 -j ACCEPT

#Faltarien totes les altres ip!!!! malgrat es permet perque es ACCEPT

# ENRUTA:
echo 1 > /proc/sys/net/ipv4/ip_forward
# Regles NAT
iptables -t nat -A POSTROUTING -s 172.31.0.0/16 -o enp5s0 -j MASQUERADE 
iptables -t nat -A POSTROUTING -s 172.32.0.0/16 -o enp5s0 -j MASQUERADE

# PORTFORWARDING
# QUAN CONECTI AL HOST:4001 -> DAYTIME DE A1
iptables -t nat -A PREROUTING -i enp5s0 -p tcp --dport 4001 -j DNAT --to 172.31.0.2:13
# host:4002 -> ssh b1
iptables -t nat -A PREROUTING -i enp5s0 -s 192.168.2.0/24 -p tcp --dport 4002 -j DNAT --to 172.32.0.2:22
# caulsevol acces host:4003 vingui don vingui vagui a a2:7
iptables -t nat -A PREROUTING -s 0.0.0.0/0 -p tcp --dport 4003 -j DNAT --to 172.31.0.3:7
# host:4004 redirigeix :22
iptables -t nat -A PREROUTING -i enp5s0 -p tcp --dport 4004 -j DNAT --to :22

iptables -A INPUT -i enp5s0 -p tcp --dport 22 -j REJECT

# Llistar
iptables -L
