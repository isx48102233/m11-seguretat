#! /bin/bash
# iptables01
# @edt ASIX-M11
# --------------------
# Regles flush: buidar regles actuals
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Establir politiques per defecte
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP
#iptables -t nat -P PREROUTING REJECT
#iptables -t nat -P POSTROUTING REJECT

# Obrir trafic propi localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Obrir trafic propi ip publica
iptables -A INPUT -s 192.168.2.35 -j ACCEPT
iptables -A OUTPUT -d 192.168.2.35 -j ACCEPT

# Obrir trafic propi ip docker
iptables -A INPUT -s 172.17.0.1 -j ACCEPT
iptables -A OUTPUT -d 172.17.0.1 -j ACCEPT

# Obrir tot trafic amb gandhi
iptables -A INPUT -s 192.168.0.10 -j ACCEPT
iptables -A OUTPUT -d 192.168.0.10 -j ACCEPT

# TRAFIC SSH: ENS PODEM CONECTAR
iptables -A OUTPUT -p tcp -d 0.0.0.0/0 --dport 22 -j ACCEPT
iptables -A INPUT -p tcp -s 0.0.0.0/0 --sport 22 -m state --state RELATED,ESTABLISHED -j ACCEPT

# EL MEU SERVEI SSH: ENS PODEN CONECTAR
iptables -A INPUT -p tcp -s 0.0.0.0/0 --dport 22 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 22 -d 0.0.0.0/0 -m state --state RELATED,ESTABLISHED -j ACCEPT

# Navegar per internet
iptables -A OUTPUT -p tcp -d 0.0.0.0/0 --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp -d 0.0.0.0/0 --dport 443 -j ACCEPT

iptables -A INPUT -p tcp -s 0.0.0.0/0 --sport 80 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -s 0.0.0.0/0 --sport 443 -m state --state RELATED,ESTABLISHED -j ACCEPT
