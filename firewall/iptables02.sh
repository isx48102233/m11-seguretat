#! /bin/bash
# iptables01
# @edt ASIX-M11
# --------------------
# Regles flush: buidar regles actuals
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Establir politiques per defecte
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# Obrir trafic propi localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Obrir trafic propi ip publica
iptables -A INPUT -s 192.168.2.41 -j ACCEPT
iptables -A OUTPUT -d 192.168.2.41 -j ACCEPT

#Faltarien totes les altres ip!!!! malgrat es permet perque es ACCEPT

# ENRUTA:
echo 1 > /proc/sys/net/ipv4/ip_forward
# Regles NAT
iptables -t nat -A POSTROUTING -s 172.31.0.0/16 -o enp5s0 -j MASQUERADE 
iptables -t nat -A POSTROUTING -s 172.32.0.0/16 -o enp5s0 -j MASQUERADE

# Forward
#1) Trafic de dins a fora
#iptables -A FORWARD -d 192.168.2.58 -i br-9f72b9dee745 -o enp5s0 -j REJECT
#iptables -A FORWARD -s 172.31.0.0/16 -i br-9f72b9dee745 -o enp5s0 -j ACCEPT
#iptables -A FORWARD -s 0.0.0.0/0 -i enp5s0 -o br-9f72b9dee745 -m state --state ESTABLISHED,RELATED -j ACCEPT
# tota xarxa a pot conectar a la xarxa 192.168.2
#iptables -A FORWARD -d 192.168.2.0/24 -i br-9f72b9dee745 -o enp5s0 -j ACCEPT
#iptables -A FORWARD -s 192.168.2.0/24 -i enp5s0 -o br-9f72b9dee745 -m state --state ESTABLISHED,RELATED -j ACCEPT
# amb accept per defecta xarxa b no pot sortir
#iptables -A FORWARD -s 172.32.0.0/16 -o enp5s0 -j REJECT
#iptables -A FORWARD -d 172.32.0.0/16 -i enp5s0 -j REJECT

#XARXA A PROHIBIT XARXA B
#iptables -A FORWARD -s 172.32.0.0/16 -d 172.31.0.0/16 -j ACCEPT
#iptables -A FORWARD -s 172.31.0.0/16 -d 172.32.0.0/16 -m state --state RELATED,ESTABLISHED -j ACCEPT

#iptables -A FORWARD -s 172.31.0.0/16 -d 172.32.0.0/16 -j REJECT

# i que A pugui tenir dialeg amb B, QUALSEVOL TRAFIC
iptables -A FORWARD -s 172.31.0.0/16 -d 172.32.0.0/16 -j ACCEPT
iptables -A FORWARD -s 172.32.0.0/16 -d 172.31.0.0/16 -m state --state RELATED,ESTABLISHED -j ACCEPT

#B NOMES A 7 I 13 DE LA A resta xapats.
iptables -A FORWARD -p tcp -s 172.32.0.0/16 -d 172.31.0.0/16 --dport 13 -j ACCEPT
iptables -A FORWARD -p tcp -s 172.31.0.0/16 -d 172.32.0.0/16 -m state --state RELATED,ESTABLISHED --sport 13 -j ACCEPT
iptables -A FORWARD -p tcp -s 172.32.0.0/16 -d 172.31.0.0/16 -j REJECT

# Llistar
iptables -L
